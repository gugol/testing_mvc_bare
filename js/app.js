(function(w) {

    'use strict';

    //MVC pattern to better achieve Separation of Concerns
    w.model={
        url: 'data/cats.json',

        cats:null,

        currentCat: null,

        error: null
    };

    w.controller={

        init: function(url){

            //only the header is visible at the beginning
            w.mainView.invisibilize();
            
            //call data
            w.controller.getData(url).then(function(succesfulResponse){
                // parse response
                w.controller.parseResponse(succesfulResponse);
                //make main div visible
                w.mainView.render();
                
            // capture errors from the success function too
            }).catch(function(error){
                //set model.error
                w.controller.setError(error.name+ " => " +error.message);
                //reset main view
                w.mainView.reset();
                //log error to trace it
                console.log(error);
            })
        },

        //get data by Promise
        getData: function (url) {
            // Return a new Promise
            return new Promise(function(resolve, reject){

                var req = new XMLHttpRequest();
                req.open('GET', url);

                req.onload = function(){ 
                    //check status
                    req.status === 200 ? resolve(req.response) : reject(Error(req.statusText + ":" + req.status));
                };

                // Handle network errors
                req.onerror = function(e){ 
                    reject(Error('Network Error: ' + e));
                };

                // Make the request
                req.send();
            })
        },

        // parse JSON to JS Object (erros controlled in controller.init function)
        parseResponse:function (jsonResponse) {
            var catsObject, catsArray;
            
            // try {
                //parse data
                catsObject=JSON.parse(jsonResponse);

                // fill model
                w.controller.setCats( catsObject );
                catsArray= w.controller.getCats();
                w.controller.setCurrentCat( catsArray[0] );

                //initialize views
                w.listView.init();
                w.detailedView.init();
            // } catch (error) {
            //     throw error;
            // }
        },

        // setters and getters
        setCats: function(data) {
            w.model.cats = data.cats;
        },

        getCats: function () {
            return w.model.cats;
        },

        setCurrentCat: function(cat) {
            w.model.currentCat = cat;
        },

        getCurrentCat: function () {
            return w.model.currentCat;
        },

        setError: function(e){
            w.model.error=e;
        },

        getError: function(){
            return w.model.error;
        },

        setUrl: function(url){
            w.model.url=url;
        },

        getUrl: function(){
            return w.model.url;
        }


    };
        

    w.listView={

        init: function() {
            // store the DOM element for easy access later
            this.catList = document.getElementById('cat--list');

            // render this listView (update the DOM elements with the right values)
            this.render();
        },

        render: function() {
            var cat, elem, innerElement, i;

            // get the cats from the model
            var cats = w.controller.getCats();;

            // empty the cat list
            this.catList.innerHTML = '';

            // loop over the cats
            for (i = 0; i < cats.length; i++) {
                
                cat = cats[i];

                // make a new cat item and set its text
                elem = document.createElement('div');
                innerElement = document.createElement('span');
                innerElement.textContent = cat.name;
                elem.appendChild(innerElement);
                elem.className = 'cat--item';

                // on click, setCurrentCat and render the detailView
                elem.addEventListener('click', (function(clickedCat) {
                    return function() {
                        if(clickedCat.name !== w.model.currentCat.name){
                            w.controller.setCurrentCat(clickedCat);
                            w.detailedView.render();
                        }
                    };
                })(cat));

                // finally, add the element to the list
                this.catList.appendChild(elem);
            }
        }
        
    };

    w.detailedView = {

        init: function() {
            // store pointers to our DOM elements for easy access later
            this.catDetails = document.getElementById('cat--details');
            this.catImage = document.getElementById('cat--img');
            this.catName = document.getElementById('cat--name');

            // render this view (update the DOM elements with the right values)
            this.render();
        },

        render: function() {
            // update the DOM elements with values from the current cat
            var currentCat = w.controller.getCurrentCat();

            this.catImage.src = currentCat.imgUrl;
            this.catName.textContent = currentCat.name;
        },
    
    };


    w.mainView = {

        invisibilize: function() {
            // store pointers to our DOM elements for easy access later
            this.main=document.getElementById('main');

            this.main.style.visibility= 'hidden';
        },


        render: function() {
            // make the main element visible
            this.main.style.visibility= 'visible';
        },
          
        reset: function() {  
            //empty the main element
            this.main.innerHTML = '';

            // update the DOM elements with values from the current model.error
            var currentError= w.controller.getError();
            this.main.textContent = currentError;
            this.main.className = 'error';

            // make the main element visible
            this.render();
        }
    };

    //init the application
    w.controller.init(w.controller.getUrl());

})(window);
