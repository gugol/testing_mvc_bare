var jsdom = require("jsdom").jsdom;

var doc = jsdom('<!DOCTYPE html> <html lang="en"> <head> <meta charset="utf-8"> <title>Cats</title> </head> <body> <div class="container"> <div class="header"> <h1>Cats</h1> </div> <div id="main" class="main"> <div id="cat--list"> </div> <div id="cat--details" > <div> <a href="#"> <img id="cat--img" alt="preview cat"> </a> </div> <div> <h3 id="cat--name"></h3> </div> </div> </div> </div> <script src=js/app.js></script> </body> </html>');

var win = doc.defaultView;

if(Object.keys(win).length === 0) {
    // this hapens if contextify, one of jsdom's dependencies doesn't install correctly
    // (it installs different code depending on the OS, so it cannot get checked in.);
    throw "jsdom failed to create a usable environment, try uninstalling and reinstalling it";
}

global.document = doc;
global.window = win;

Object.keys(window).forEach(function (key) {
  if (!(key in global)) {
    global[key] = window[key];
  }
});



