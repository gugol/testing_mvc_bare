describe("app", function() {

  'use strict';

  var app=require('../js/app');  
  
  var m, c, lv, dv, mv, data, intialCat;

  beforeEach(function() {
    m = window.model;
    c = window.controller;
    lv = window.listView;
    dv= window.detailedView;
    mv= window.mainView;

    data = {
    cats: [
          {
              name: "Bill",
              imgUrl: "imgs/Bill.jpg"
          },
          {
              name: "Sandy",
              imgUrl: "imgs/Sandy.jpg"
          },
          {
              name: "Basel",
              imgUrl: "imgs/Basel.jpg"
          },
          {
              name: "Catman",
              imgUrl: "imgs/Catman.jpg"
          }
          ,
          {
              name: "Tomcat",
              imgUrl: "imgs/Tomcat.jpg"
          }
      ]
  };

    intialCat = {
        name: "Bill",
        imgUrl: "imgs/Bill.jpg"
    };
    
  });


  it("should be able to get the model.url", function() {
    // vars
    var intialUrl;
    
    //get
    intialUrl = c.getUrl();

    //expects
    expect(intialUrl).toEqual(jasmine.any(String));    
  });

  it("should be able to set the model.url", function() {
    // vars
    var newUrl;
    
    newUrl= "http://jsonstub.com/lm/cats";

    //set 
    c.setUrl(newUrl);

    //expects
    expect(newUrl).toEqual(c.getUrl());
  });


  it("should be able to get the model.error", function() {
    // vars
    var intialErr;
    
    //get
    intialErr = c.getError();

    //expects
    expect(intialErr).toBe(null);    
  });

  it("should be able to get/set the model.error", function() {
    // vars
    var newErr;
    
    newErr= "Error => Server Error : 500";

    //set
    c.setError(newErr);

    //expects
    expect(newErr).toEqual(c.getError());
  });

  //currentCat
  it("should be able to get the model.currentCat", function() {
    // vars
    var intialCurrentCat;
    
    //get
    intialCurrentCat = c.getCurrentCat();

    //expects
    expect(intialCurrentCat).toBe(null);    
  });

  it("should be able to get/set the model.currentCat", function() {
      // vars
      var newCurrentCat;

      newCurrentCat= {
          name: "Bill",
          imgUrl: "imgs/Bill.jpg"
      };

      //set
      c.setCurrentCat(newCurrentCat);

      //expects
      expect(newCurrentCat).toEqual(c.getCurrentCat());
  });

//cats
  it("should be able to get the model.cats", function() {
    // vars
    var intialCats;
    
    //get
    intialCats = c.getCats();

    //expects
    expect(intialCats).toBe(null);    
  });

  it("should be able to get/set the model.cats", function() {
    // vars
    var dataCats;
    
    dataCats= { 
        cats : [
            {
              name: "Bill",
              imgUrl: "imgs/Bill.jpg"
          },
          {
              name: "Sandy",
              imgUrl: "imgs/Sandy.jpg"
          }
        ]
    };

    //set
    c.setCats(dataCats);

    //expects
    expect(dataCats.cats).toEqual(c.getCats());
  });


  it("should be parse a JSON Object succesfully", function() {
    // vars
    var dataCats;

    // stringify JS Object
    dataCats=JSON.stringify(data);

    spyOn(c, 'setCats').and.callThrough();
    spyOn(c, 'getCats').and.callThrough();
    spyOn(c, 'setCurrentCat').and.callThrough();

    spyOn(lv, 'init');
    spyOn(dv, 'init');

    //call
    c.parseResponse(dataCats);

    //expects
    expect(c.setCats).toHaveBeenCalledWith(data);
    expect(c.getCats).toHaveBeenCalled();
    expect(c.setCurrentCat).toHaveBeenCalledWith(data.cats[0]);

    expect(lv.init).toHaveBeenCalled();
    expect(dv.init).toHaveBeenCalled();

    expect(data.cats).toEqual(c.getCats());
  });

  it("should throw a SyntaxError when parsing a not valid JSON cat Object", function() {

    spyOn(c, "setCats").and.callThrough();
    spyOn(c, "getCats").and.callThrough();
    spyOn(c, "setCurrentCat").and.callThrough();

    spyOn(lv, "init");
    spyOn(dv, "init");

    //call
    expect( function(){ c.parseResponse(undefined); } ).toThrowError(SyntaxError);

    //expects
    expect(c.setCats).not.toHaveBeenCalledWith(data);
    expect(c.getCats).not.toHaveBeenCalled();
    expect(c.setCurrentCat).not.toHaveBeenCalledWith(data.cats[0]);

    expect(lv.init).not.toHaveBeenCalled();
    expect(dv.init).not.toHaveBeenCalled();
  });

  // AJAX Promise getData
  describe('JS ajax call getData', function () {
    var xhr;
    var urls;

    beforeEach(function () {
      xhr = {
        open: function open(_type_, _url_) {
          xhr.status = urls[_url_].status;
          xhr.response = urls[_url_].response;
          xhr.statusText = urls[_url_].statusText;
        },
        send: function send() {
          setTimeout(xhr.onload, 1);
        }
      };

      XMLHttpRequest = jasmine.createSpy(xhr).and.returnValue(xhr);
    });

    it('resolves AJAX call with `response`', function (done) {
      urls = {
        'http://validurl': {
          response: 'I am a good response',
          status: 200
        }
      };

      c.getData('http://validurl').then(function (response) {
        return expect(response).toBe('I am a good response');
      }, function (error) {
        return expect(error).toBe(undefined);
      }).then(done);
    });

    it('rejects AJAX call with `Error(req.statusText + ":" + req.status)`', function (done) {
      urls = {
        'http://notvalidurl': {
          response: 'It does not matter, I will never reach my caller',
          status: 500,
          statusText: 'Error message'
        }
      };

      c.getData('http://notvalidurl').then(function (response) {
        return expect(response).toBe(undefined);
      }, function (error) {
        return expect(error.message).toBe('Error message'+':'+'500');
      }).then(done);
    });
  });

  describe('init', function() {

    var promise;

    beforeEach(function() {
      promise = {
        deffered: function() {
          return (new function Deffered() {
            var self = this
            this.promise = new Promise(function(resolve, reject) {
              self.resolve = resolve;
              self.reject = reject
            })
          }())
        }
      }
    });

    it('calls `controller.parseResponse` on resolve', function(done) {
      var dfd = promise.deffered()

      spyOn(c, 'getData').and.returnValue(dfd.promise);
      spyOn(c, 'parseResponse');
      spyOn(c, 'setError');
      spyOn(mv, "render");

      c.init('validUrl');

      dfd.resolve('I am a good response');

      setTimeout(function() {
        expect(c.setError).not.toHaveBeenCalled();
        expect(c.parseResponse).toHaveBeenCalledWith('I am a good response');
        expect(mv.render).toHaveBeenCalled();  
        done()
      })
    })

    it('calls `controller.setError` on rejection', function(done) {
      var dfd = promise.deffered()

      spyOn(c, 'getData').and.returnValue(dfd.promise);
      spyOn(c, 'parseResponse');
      spyOn(c, 'setError');
      spyOn(mv, "reset");

      c.init('notValidUrl');

      dfd.reject({
        name: 'Error name',
        message: 'Error message'
      });

      setTimeout(function() {
        expect(c.parseResponse).not.toHaveBeenCalled();
        expect(c.setError).toHaveBeenCalledWith('Error name => Error message');
        expect(mv.reset).toHaveBeenCalled();  
        done()
      })

    })
  })

});


